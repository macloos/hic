function [f0, data] = getFreq(epr,r0,r1,r2)
    data = zeros(1500,2);

    for i = 1:1500

        f = (123.0 + (-100 + 0.2*i)) * 10^6;
        [~,Y] = getAdmittance(f, epr, r0,r1, r2);
        data(i,1) = f;
        data(i,2) = Y;
    end

    [~,index]= max(data(:,2));
    f0 = max(data(index,1));
end