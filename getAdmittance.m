function [Z,Y] = getAdmittance(f,epr,r0,r1,r2)

ep0 = 8.85418781 * 10^-12;
mu0 = 4*pi * 10^-7;
cc0 = 1.0/sqrt(ep0*mu0);

% epr =2.2;
% 
% r0=40.0 * 10^-3;
% r1= 2.4 * 10^-3;
% r2= 0.5 * 10^-3;

l = 2*pi*r0;

%f = 123.0 * 10^6;
w = 2*pi*f;  
 

Z0 = 1/(2*pi) * sqrt(mu0/(ep0*epr)) * log(r1/r2);
Zl = 1i*w*mu0*r0*(log(8*r0/r1)-2);
Zc = -1i*Z0*cot(w*l*sqrt(epr)/cc0);

Z = Zl + 2*Zc;
Y = 1/Z;

end