clear all; close all;

epr =2.1;

r0=40.0 * 10^-3;
r1= 2.86 * 10^-3;
r2= 0.5 * 10^-3;

[f0,data] = getFreq(epr, r0, r1, r2);

figure; plot(data(:,1),abs(data(:,2)))
% figure; plot(data(:,1),imag(1/data(:,2)))
f0
%% find trace

f0mapPolar = zeros(90,2);
f0mapCar = zeros(360,2);


for an=1:90    
    eprN = epr* (1+0.01*cos(an*pi/180));
    r0N  = r0 * (1+0.01*sin(an*pi/180));
    
%     f0mapPolar(an,1) = an;
%     f0mapPolar(an,2) = getFreq(eprN, r0N, r1, r2);
    
    fN = getFreqFine(eprN, r0N, r1, r2);
    
    delta = abs(f0- fN)/(1.0e+06);
    f0mapCar(an,1) =  cos(an*pi/180)*delta;
    f0mapCar(an,2) =  sin(an*pi/180)*delta;
    
end

for an=91:180    
    r1N  = r1* (1+0.01*cos(an*pi/180));
    r0N  = r0 * (1+0.01*sin(an*pi/180));
    
%     f0mapPolar(an,1) = an;
%     f0mapPolar(an,2) = getFreq(eprN, r0N, r1, r2);
    
    fN = getFreqFine(epr, r0N, r1N, r2);
    
    delta = abs(f0- fN)/(1.0e+06);
    f0mapCar(an,1) =  cos(an*pi/180)*delta;
    f0mapCar(an,2) =  sin(an*pi/180)*delta;
    
end


for an=181:270    
    r1N  = r1* (1+0.01*cos(an*pi/180));
    r2N  = r2* (1+0.01*sin(an*pi/180));
    
%     f0mapPolar(an,1) = an;
%     f0mapPolar(an,2) = getFreq(eprN, r0N, r1, r2);
    
    fN = getFreqFine(epr, r0, r1N, r2N);
    
    delta = abs(f0- fN)/(1.0e+06);
    f0mapCar(an,1) =  cos(an*pi/180)*delta;
    f0mapCar(an,2) =  sin(an*pi/180)*delta;
    
end


for an=271:360    
    eprN = epr*(1+0.01*cos(an*pi/180));
    r2N  = r2* (1+0.01*sin(an*pi/180));
    
%     f0mapPolar(an,1) = an;
%     f0mapPolar(an,2) = getFreq(eprN, r0N, r1, r2);
    
    fN = getFreqFine(eprN, r0, r1, r2N);
    
    delta = abs(f0- fN)/(1.0e+06);
    f0mapCar(an,1) =  cos(an*pi/180)*delta;
    f0mapCar(an,2) =  sin(an*pi/180)*delta;
    
end
csvwrite('fig_div/123_1p.table',f0mapCar);


