clear all; close all;

epr =2.2;

r0=40.0 * 10^-3;
r1= 2.4 * 10^-3;
r2= 0.5 * 10^-3;

[f0,data] = getFreq(epr, r0, r1, r2);

figure; plot(data(:,1),abs(data(:,2)))
% figure; plot(data(:,1),imag(1/data(:,2)))
f0;
%% find trace

f0map = zeros(100,10000);
table = zeros(100,10000,4);

r2  = 0.5;
epr = 2.2;

h = waitbar(0,'running');
for r0=1:100
    for r1=1:10000
        
        r1s = r2  * 10^-3 + r1  * 0.1e-4;
        
        table(r0,r1, 1) = r0  * 10^-3;
        table(r0,r1, 2) = r1s;
        table(r0,r1, 3) = r2  * 10^-3;
        table(r0,r1, 4) = epr;
        
        f0map(r0,r1) = getFreq(epr,r0  * 10^-3, r1s , r2  * 10^-3 );
    end
    waitbar(r0/100);
end

close (h);

%% extract 1.5T
data1p5T = zeros(100,4);

test =abs(f0map - 64.0e+06);

last = 0 * squeeze(table(1,1,:));
last(2) = r2  * 10^-3;
for i=1:100
    [v,pos] = min(test(i,:));
    
    if(v < 0.5e+06)
        data1p5T(i,:) = squeeze(table(i,pos,:));
        last = squeeze(table(i,pos,:));
    else
         data1p5T(i,:) = last;      
    end
end

tosave = data1p5T(:,1:2).*1000;
figure(1);
plot(tosave(:,1),tosave(:,2)); title('1.5T')


%csvwrite('fig/64MHzEpr2p2.table',tosave);




%% extract 3T
data3T = zeros(100,4);

test =abs(f0map - 123.0e+06);

last = 0 * squeeze(table(1,1,:));
last(2) = r2  * 10^-3;
for i=1:100
    [v,pos] = min(test(i,:));
    
    if(v < 0.5e+06)
        data3T(i,:) = squeeze(table(i,pos,:));
        last = squeeze(table(i,pos,:));
    else
         data3T(i,:) = last;      
    end
end

tosave = data3T(:,1:2).*1000;


figure(2);
plot(tosave(:,1),tosave(:,2)); title('3T')

%csvwrite('fig/123MHzEpr2p2.table',tosave);


%% extract 7T
data7T = zeros(100,4);

test =abs(f0map - 298.0e+06);

last = 0 * squeeze(table(1,1,:));
last(2) = r2  * 10^-3;
for i=1:100
    [v,pos] = min(test(i,:));
    
    if(v < 0.5e+06)
        data7T(i,:) = squeeze(table(i,pos,:));
        last = squeeze(table(i,pos,:));
    else
         data7T(i,:) = last;      
    end
end

tosave = data7T(:,1:2).*1000;

figure(3);
plot(tosave(:,1),tosave(:,2)); title('7T')

%csvwrite('fig/298MHzEpr2p2.table',tosave);

save('f0mapEPR2p2.mat','f0map','-v6')
