function [f0, data] = getFreqFine(epr,r0,r1,r2)
    data = zeros(15000,2);

    for i = 1:15000

        f = (123.0 + (-100 + 0.02*i)) * 10^6;
        [~,Y] = getAdmittance(f, epr, r0,r1, r2);
        data(i,1) = f;
        data(i,2) = Y;
    end

    [~,index]= max(data(:,2));
    f0 = max(data(index,1));
end